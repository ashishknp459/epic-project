<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Service\GetdataService;
class KanyeController extends Controller
{

    protected $GetdataService;
    public function __construct(GetdataService $GetdataService){
        $this->GetdataService=$GetdataService;
    }

    public function index(Request $request){
        return view('kanye');
    }
    public function get_kanye(Request $request){
        $response=$this->GetdataService->index();
        return Response::json($response);
    }   
}
