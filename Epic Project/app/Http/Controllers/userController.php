<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use DB;
use Auth;
use Hash;
use Session;
class userController extends Controller
{
    public function index(Request $request){
       
        return view('user');
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required|string|max:250',
            'email' => 'required|email|max:250|unique:users',
            'password' => 'required|min:8',
            'mobile_number' => 'required|numeric|digits:10'
        ]);

            $savedata=new User();
            $savedata->name= $request->name;
            $savedata->email = $request->email;
            $savedata->password = Hash::make($request->password);
            $savedata->mobile=$request->mobile_number;
            $savedata->save();

        
        $credentials = $request->only('email', 'password');
        Auth::attempt($credentials);
        // $request->session()->regenerate();
        return redirect()->route('index')
        ->withSuccess('You have successfully registered & logged in!');
    }
    public function login(Request $request){
       
        $request->validate([
            'email' => 'required|email|max:250',
            'password' => 'required|min:8',
        ]);
        $credentials = $request->only('email', 'password');
        Auth::attempt($credentials);
        return redirect()->route('kanye')
        ->withSuccess('You have successfully logged in!');
    }
   public function logout(){
    Session::flush(); 
    Auth::logout();

    return redirect()->route('auth.login');
   }
    public function save(Request $request){
        if(isset($request->edit_id)&&!empty($request->edit_id)){
            $savedata=User::where('id', '=',  $request->edit_id)->first();
            }else{
            $savedata=new User();
            }
        $savedata->name=$request->student_name;
        $savedata->email=$request->email;
        $savedata->mobile=$request->mobile_number;
        $image=$request->image;

        if($image){
            if(!empty($request->edit_id)){

                $image_path=DB::table('users')->select('image')->where('id', $request->edit_id)->first();
                $path=(array)$image_path;
    
                if($path['image']){
                    $image_path=public_path($path['image']);
                    if(file_exists($image_path)){
                    unlink($image_path);
                }
                }
            }

                $extension=$image->getClientOriginalExtension();
                $name=$image->getClientOriginalName();
                $size=$image->getSize();
                $destinationPath = public_path('/uploads/user');
                $createname=date('d-m-y').time() . $name;
                $image->move($destinationPath, $createname);

        $savedata->image='/uploads/user/' . $createname;
        }
        $savedata->save();

        $response=array(
            'status'=>'success',
            'message'=>'Successfully Submitted'
        );
        return $response;
    }

    // ______________________________________________
    //| This function is edit                        |
    //| You can Edit our code from here              |
    //|______________________________________________|
    public function edit(Request $request){
        $id=$request->input;
    	if(!empty($id))
    	{
			
    		$row=User::where('id',$id)->get()->toarray();
    	
			$request['status']=1;
            $request['error']=false;
            $request['message']=null;
            $request['data']=$row[0];
          
    	}else{
    		$request['status']=1;
            $request['error']=true;
            $request['message']='No data found';
            $request['data']=null;
           
    	}
        return $request;
    }
    public function deletedata(Request $request){
        $id=$request->input;
    	if(!empty($id))
    	{	
    		User::where('id',$id)->delete();
    		$request['status']=1;
            $request['error']=false;
            $request['message']='delete successfully';
            $request['data']=null;
           
    	}else{
    		$request['status']=1;
            $request['error']=true;
            $request['message']='No data found';
            $request['data']=null;
           
    	}
        return $request;
    }
    public function all_select(Request $request){

        $column = array('id','name','image','mobile','email','id');
        $query = " SELECT * FROM users ";
        $query .=" WHERE id > 0";
      
        if(Auth::User()->type=='user'){
            $query.=" AND id=".Auth::User()->id;
        }
        if(isset($request['search']))
        {
            $query.=" AND ( email LIKE '%".$request['search']['value']."%' OR name LIKE '%".$request['search']['value']."%' OR mobile LIKE '%".$request['search']['value']."%' OR id LIKE '%".$request['search']['value']."%' ) ";
        }
      if(isset($request['order']))
      {
         $query .=' ORDER BY '.$column[$request['order']['0']['column']].' '.$request['order']['0']['dir'].' ';
      }
      else
      {
         $query .= ' ORDER BY id DESC ';
      }
//   var_dump($query);
      $number_filter_row= count(DB::select($query));
    
      if(isset($request["length"])&&$request["length"] != -1)
      {
         $query.= ' LIMIT ' . $request['start'] . ', ' . $request['length'];
      }

        $result =DB::select($query);
        $data = array();
        foreach($result as $key=> $value)
        {
          $action=' <a class="btn btn-success btn-sm" href="javascript:void(0);" onclick="view(`'.$value->id.'`);" data-toggle="tooltip" data-placement="top" title="" data-original-title="View"><i class="fa fa-eye"></i></a>';
          $action.=' <a class="btn btn-primary btn-sm" href="javascript:void(0);" onclick="edit(`'.$value->id.'`);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-edit"></i></a>';
          if(Auth::User()->type=='admin'){
              $action.=' <a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick="deleted(`'.$value->id.'`);" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="fa fa-trash"></i></a>';
          }
          
          $sub_array = array();
          $sub_array[] = ++$key;	
          $sub_array[] = '<img src="'.$value->image.'" alt="Not Found" width="30px" height="30px">';
          $sub_array[] = $value->name;
          $sub_array[] = $value->email;
          $sub_array[] = $value->mobile;
          $sub_array[] =  $action;
          $data[] = $sub_array;
        }
      $output = array(
         "draw"       =>  intval($request["draw"]),
         "recordsTotal"   =>  $this->count_all_staff_list(),
         "recordsFiltered"  =>  $number_filter_row,
         "data"       =>  $data
        );
 
    //   echo json_encode($output);

        return $output;
    }

	public function count_all_staff_list()
    {
	    $query = "SELECT * FROM users ";
	    $query .=" WHERE id > 0 ";
	    $row= count(DB::select($query));
	    return $row;
    }


}
