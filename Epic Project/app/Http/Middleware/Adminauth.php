<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Session;


class Adminauth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next,string $admin_auth)
    {       
        $user_type=Auth::User()->type;
 
                if(strpos($admin_auth,$user_type)){
                   
                    return $next($request);
                }else if($admin_auth=='Type|user'){
                    return redirect()->route('studentdata')->with('error','you have to be logged in to access this page');
                }
                 return redirect('/register')->with(['status'=>'failed','msg'=>'you have to be logged in to access this page']);
    }

}
