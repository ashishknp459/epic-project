<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Service\GetdataService;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton('App\Service\GetdataService', function ($app) {
            return new GetdataService(
                 
                   );
          });

    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
