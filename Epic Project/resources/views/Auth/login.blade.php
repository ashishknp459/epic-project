@extends('master/main')
@extends('master/header')
@extends('master/footer')
@section('content')
<form action="{{route('login')}}" method="post" id="loginForm">
    @csrf
    <div class="d-flex align-items-center justify-content-center ht-100v">
        <div class="overlay-body bg-black-6 d-flex align-items-center justify-content-center">
            <div class="login-wrapper wd-300 wd-xs-350 pd-25 pd-xs-40 rounded bd bd-white-2 bg-black-7">
                <div class="tx-black tx-center">Welcome Back</div>
                <div class="signin-logo tx-center mg-b-20 tx-28 tx-bold tx-black"><span class="tx-normal"></span>
                    Epic<span class="tx-info"> Project</span> <span class="tx-normal"></span></div>

                <div class="form-group">
                    <input type="text" class="form-control fc-outline-light" name="email" placeholder="Username">
                    @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                    @endif
                </div><!-- form-group -->
                <div class="form-group">
                    <input type="password" class="form-control fc-outline-light" name="password" placeholder="Password">
                    @if ($errors->has('password'))
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                    @endif
                </div><!-- form-group -->
                <button type="submit" class="btn btn-info btn-block">Log In</button>
            </div><!-- login-wrapper -->
        </div><!-- overlay-body -->
    </div><!-- d-flex -->
</form>
<div id="loaders"
    style="z-index: 9999;position: fixed;top:0%;width: 100%;height: 100%;z-index: 9999;text-align: center;background: #1f1b1721; display:none; ">
</div>

@endsection