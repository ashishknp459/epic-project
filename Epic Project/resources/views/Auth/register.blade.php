@extends('master/main')
@extends('master/header')
@extends('master/footer')
@section('content')

<form action="{{route('store')}}" method="post" id="loginForm">
    <div class="d-flex align-items-center justify-content-center ht-100v">
        <div class="overlay-body bg-black-6 d-flex align-items-center justify-content-center">
            <div class="login-wrapper wd-600 wd-xs-650 pd-25 pd-xs-40 rounded bd bd-white-2 bg-black-7">
                <div class="tx-black tx-center">Welcome Back</div>
                <div class="signin-logo tx-center mg-b-20 tx-28 tx-bold tx-black"><span class="tx-normal"></span>
                    Epic<span class="tx-info"> Project</span> <span class="tx-normal"></span></div>

                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" placeholder="Please Enter Name "
                                    id="name">
                                @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type='email' class="form-control" name="email"
                                    placeholder="Please Enter Email Id " id="email">
                                @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="password"
                                    placeholder="Enter Password " id="password">
                                @if ($errors->has('password'))
                                <span class="text-danger">{{ $errors->first('password') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12  ">
                            <div class="form-group">
                                <label for="mobile_number">Mobile Number</label>
                                <input type='number' class="form-control" name="mobile_number"
                                    placeholder="Enter Mobile Number " id="mobile_number">
                                @if ($errors->has('mobile_number'))
                                <span class="text-danger">{{ $errors->first('mobile_number') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <button type="submit" id="register" class="btn btn-primary">Save</button>
                    </div>
                </div>
             
</form>

</div>

<script>
$(document).ready(function() {
    $('#manageModal').modal('show');
});
</script>
<!-- internal staff=>
customer=>
vendor/busines patner=>
external staff=>
 -->

<!-- END content-page -->

@endsection