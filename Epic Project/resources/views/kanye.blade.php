@extends('master/main')
@extends('master/header')
@extends('master/headerLayout')
@extends('master/footer')
@extends('master/leftsidebar')
@section('content')

<div class="card">
    <div class="card-header">
      <a href="http://{{request()->getHttpHost()}}/api/auth/kanye">{{request()->getHttpHost()}}/api/auth/kanye</a>
    <a class="pull-right btn btn-sm" href="javascript:void(0);" onclick="getkanye();" data-toggle="tooltip" data-placement="top" title="" data-original-title="Refresh"><i class="fas fa-refresh" ></i> &nbsp;&nbsp;Refresh</a>
    </div>
    <div class="card-body">
        <card class="head text-center m-2">
           <p> <b id="htmlcode"></b><p>
        </card>
    </div>
</div>
<script>
$(document).ready(function(){
    getkanye();
});

function getkanye()
  {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
          type:'post', 
          url: '/dashboard/kanye/get_kanye',
          dataType:'json',
          beforeSend:function()
          {
            $('#preloader').css('display','block');
          },
          success:function(responce)
          {
            if(responce.error)
            {
              $.notify(responce.message, "warn",{arrowSize: 20});
            }else
            {
              $("#htmlcode").html('"'+responce+'"');
            }  
          },
          error:function()
          {
            $('#preloader').css('display','none');
            $.notify("BOOM....!", "error");
          },
          complete:function()
          {
            $('#preloader').css('display','none');
          }
      });
  }


</script>

@endsection