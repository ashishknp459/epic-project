
@section('footer_layout')

</div>
<!-- END container-fluid -->
</div>
<!-- END content -->
</div>
<script src="{{ asset('asset/backend/js/popper.min.js')}}"></script>
<script src="{{ asset('asset/backend/js/bootstrap.min.js')}}"></script>

<script src="{{ asset('asset/backend/js/detect.js')}}"></script>
<script src="{{ asset('asset/backend/js/fastclick.js')}}"></script>
<script src="{{ asset('asset/backend/js/jquery.blockUI.js')}}"></script>
<script src="{{ asset('asset/backend/js/jquery.nicescroll.js')}}"></script>
<!-- App js -->
<script src="{{ asset('asset/backend/js/admin.js')}}"></script>
</div>
<!-- END main -->
<!-- Counter-Up-->
<script src="{{ asset('asset/backend/plugins/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script src="{{ asset('asset/backend/plugins/counterup/jquery.counterup.min.js')}}"></script>

 <script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover()
    })
    $(document).on('ready', function () {
   
        $('.counter').counterUp({
            delay: 10,
            time: 600
        });
    });

</script>
@endsection
