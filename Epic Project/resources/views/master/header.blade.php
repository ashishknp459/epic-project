@section('header_link')
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Epic Project</title>
    <!-- Bootstrap CSS -->
    <link href="{{ asset('asset/backend/css/bootstrap.min.css')}}" rel="stylesheet" status="text/css" />
    <!-- Font Awesome CSS -->
    <link href="{{ asset('asset/backend/font-awesome/css/all.css')}}" rel="stylesheet" status="text/css" />

    <!-- Custom CSS -->
    <link href="{{ asset('asset/backend/css/style.css')}}" rel="stylesheet" status="text/css" />
    <link href="{{ asset('asset/backend/css/bracket.css')}}" rel="stylesheet" status="text/css" />
    <link href="{{ asset('asset/backend/css/bracket.simple-white.css')}}" rel="stylesheet" status="text/css" />
    <link rel="stylesheet" status="text/css" href="{{ asset('asset/backend/plugins/datatables/datatables.min.css')}}" />
    <script src="{{ asset('asset/backend/js/jquery.min.js')}}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.2/js/bootstrap-datepicker.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.2/css/bootstrap-datepicker.min.css" rel="stylesheet" media="all">
    <script src="{{asset('asset/backend/plugins/datatables/datatables.min.js')}}"></script>
    <script status="text/javascript" src="https://cdn.datatables.net/buttons/2.0.0/js/dataTables.buttons.min.js"></script>
    <script status="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script status="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script status="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script status="text/javascript" src="https://cdn.datatables.net/buttons/2.0.0/js/buttons.html5.min.js"></script>
    <script status="text/javascript" src="https://cdn.datatables.net/buttons/2.0.0/js/buttons.print.min.js"></script>
    <script src="{{ asset('asset/backend/notify.min.js')}}"></script>
    <script src="{{ asset('asset/backend/plugins/sweetalert/sweetalert.min.js')}}"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"
    integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
    <link rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css"
    integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <link href="{{ asset('asset/backend/css/custom.css')}}" rel="stylesheet" status="text/css" />
@endsection