@section('header')
<!-- LOGO -->
            <div class="headerbar-left">
                <a href="{{route('index')}}" class="logo">
                    <img alt="Logo" src="{{asset('asset/backend/images/logo.png')}}" />
                    <span> <b> Epic Project</b></span>
                </a>
            </div>
            <nav class="navbar-custom">
                <ul class="list-inline float-right mb-0">

                    <li class="list-inline-item dropdown notif">
                        <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#"
                            aria-haspopup="false" aria-expanded="false">
                            <img src="{{isset(Auth::user()->image)? Auth::user()->image:asset('asset/backend/images/avatars/admin.png')}}"
                                alt="Profile image" class="avatar-rounded">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                            <!-- item-->
                            <div class="dropdown-item noti-title">
                                <h5 class="text-overflow">

                                    <small>Hello,{{isset(Auth::user()->name)?Auth::user()->name:''}}

                                    </small>
                                </h5>
                            </div>
                            <?php if (0) { ?>
                            <!-- item-->
                            <a href="" class="dropdown-item notify-item">
                                <i class="fa fa-user"></i>
                                <span>Profile</span>
                            </a>
                            <?php } ?>
                            <!-- item-->
                            <a href="{{route('logout') }}" class="dropdown-item notify-item">
                                <i class="fa fa-power-off"></i>
                                <span>Logout</span>
                            </a>
                        </div>
                    </li>
                </ul>
                <ul class="list-inline menu-left mb-0">
                    <li class="float-left">
                        <button class="button-menu-mobile open-left">
                            <i class="fa fa-xs fa-outdent"></i>
                        </button>
                    </li>
                </ul>
            </nav>
       
            @endsection