@section('leftsidebar')
    <!-- Left Sidebar -->
            <div class="left main-sidebar">
                <div class="sidebar-inner leftscroll">
                    <div id="sidebar-menu">
                        <?php
                        $menu = Request::segment(2)
                        ?>
                        <ul>
                            <li class="submenu">
                                <a class="<?php echo $menu == '' ? 'active' : ''; ?>" href="{{route('kanye')}}">
                                    <i class="fa fa-tachometer"></i>
                                    <span>Kanye</span>
                                </a>
                            </li>
                            <li class="submenu">
                                <a class="<?php echo $menu == 'user' ? 'active' : ''; ?>" href="{{route('index')}}">
                                    <i class="fa fa-users"></i>
                                    <span>User</span>
                                </a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                        <!-- Bottom Fix Menu Start -->
                    </div>
                    <!-- Bottom Fix Menu End-->
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- End Sidebar -->

        
      <div id="loaders"
        style="z-index: 9999;position: fixed;top:0%;width: 100%;height: 100%;z-index: 9999;text-align: center;background: #1f1b1721; display:none; ">
        <img src="{{asset('assets/backend/loader.gif')}}" style="position: sticky;width:10%;top:40%;">
      </div>
      <div class="content-page">
        <!-- Start content -->
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-xl-12">
                <div class="breadcrumb-holder">
                  <h1 class="main-title float-left">
                    {{Request::segment(2)}}
                  </h1>
                  <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item">
                    {{Request::segment(1)}}
                    </li>
                    <li class="breadcrumb-item active">
                     {{Request::segment(2)}}
                    </li>
                  </ol>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
            <!-- end row -->
            
@endsection