<!DOCTYPE html>
<html lang="en">

<head>
    @yield('header_link')
    
</head>

<body class="adminbody">
    <div id="main">
        <!-- top bar navigation -->
    <div class="headerbar">
      @yield('header')
    </div>
    @yield('leftsidebar')
    @yield('content')
    @yield('footer_layout')
       
</body>

</html>