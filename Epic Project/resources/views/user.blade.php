@extends('master/main')
@extends('master/header')
@extends('master/headerLayout')
@extends('master/leftsidebar')
@extends('master/footer')
@section('content')

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
    <div class="card mb-3">
      <div class="card-header">
        @if(Auth::User()->type=='admin')
        <a class="pull-right btn btn-sm btn-info" href="javascript:void(0);" onclick="addNew();" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add New">Add New</a>
        @endif  
      <h3><i class="fa fa-table"></i> Data List</h3>
      </div>

      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div id="dataTableButtons"></div>
          </div>
        </div>
        <br>
        <div class="table-responsive">
          <table id="dataTable" class="table table-bordered">
            <thead>
              <tr>
                <th>ID</th>
                <th>Image</th>
                <th>Name</th>
                <th>Email</th>
                <th>Mobile</th>
                <th>Action</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade custom-modal" id="manageModal" tabindex="-1" role="dialog" aria-labelledby="manageModal" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="">Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/dashboard/user/save" method="post" id="manageForm" autocomplete="off">
        <div class="modal-body">
          <div class="row">
         
            <div class="col-md-6">
              <div class="form-group">
                <label for="student_name">Name</label>
                <input type="text" class="form-control" name="student_name" id="student_name" >
                <input type="hidden" name="edit_id" id="edit_id" value="">
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="image">Image</label>
                <input type="file" class="form-control" name="image" id="image" >
              </div>
            </div>
        </div>
        
         <div class="row">
         <div class="col-md-6">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type='email' class="form-control" name="email" id="email">
                       
                </div>
            </div>
         <div class="col-md-6">
                <div class="form-group">
                    <label for="mobile_number">Mobile Number</label>
                    <input type='number' class="form-control" name="mobile_number" id="mobile_number">  
                </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center"> 
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
        <div class="modal-footer ">
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  get_table_list();
  function get_table_list() {
    $('#dataTable').DataTable().clear().destroy();
    var table = $('#dataTable').DataTable({
      processing: true,
      serverSide: true,
      order: [[0, 'desc']],
      language: {
        "emptyTable": "No Data Available"
        },
      ajax:{url:"/dashboard/user/all_select",
            type:'get',
            data:{
            },
          },
      fnRowCallback: function( nRow, aData, iDisplayIndex ) { 
          $('td:eq(1)', nRow).css('width','10%');
        }
    });
    var buttons = new $.fn.dataTable.Buttons(table, {
     buttons: [
            { extend: 'copy' },
            { extend: 'excel' },
            { extend: 'csv' },
            { extend: 'pdf' },
            { extend: 'print' },
            //{ extend: 'pdf', orientation: 'landscape', pageSize: 'LEGAL' },
            ]  
    }).container().appendTo($('#dataTableButtons')); 
    setTimeout(function(){ $('[data-toggle="tooltip"]').tooltip('update'); }, 1000);  
  }


  function addNew()
    {

        $('#edit_id').val('');
        $('#manageModal').modal('show');
    }

    function edit(input)
  {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
          type:'post', 
          url: '/dashboard/user/edit',
          dataType:'json',
          data: {input:input}, 
          beforeSend:function()
          {
            $('#preloader').css('display','block');
          },
          success:function(responce)
          {
            if(responce.error)
            {
              $.notify(responce.message, "warn",{arrowSize: 20});
            }else
            {
              $('#edit_id').val(responce.data.id);
              $('#student_name').val(responce.data.name);
              $('#email').val(responce.data.email);
              $('#mobile_number').val(responce.data.mobile);
              $('#manageModal').modal('show');
            }  
          },
          error:function()
          {
            $('#preloader').css('display','none');
            $.notify("BOOM....!", "error");
          },
          complete:function()
          {
            $('#preloader').css('display','none');
          }
      });
  }
  function deleted(input)
  {
    swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this data!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
      .then((willDelete) => {
          if (willDelete) {

            $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                      type:'post', 
                      url: '//dashboard/user/user/deleted',
                      dataType:'json',
                      data: {input:input}, 
                      beforeSend:function()
                      {
                        $('#preloader').css('display','block');
                      },
                      success:function(responce)
                      {
                        if(responce.error)
                        {
                          $.notify(responce.message, "warn",{arrowSize: 20});
                        }else
                        {
                          $.notify(responce.message, "success",{arrowSize: 20});
                          get_table_list();
                        }  
                      },
                      error:function()
                      {
                        $('#preloader').css('display','none');
                        $.notify("BOOM....!", "error");
                      },
                      complete:function()
                      {
                        $('#preloader').css('display','none');
                      }
                  });
          }
      });
  }
  
</script>

<script type="text/javascript">
   $("#manageForm").submit(function(event) {
      event.preventDefault();
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
      $.ajax({
              type: $(this).attr('method'), 
              url: $(this).attr('action'),
              dataType:'json',
              cache: false,
              contentType: false,
              processData: false,
              data: new FormData(this), 
              beforeSend:function()
              {
                $('#preloader').css('display','block');
              },
              success:function(responce)
              {
                if(responce.error)
                {
                  $.notify(responce.message, "warn",{arrowSize: 20});
                }else
                {
                  document.getElementById('manageForm').reset();
                  $('#manageModal').modal('hide');
                  $.notify(responce.message, "success",{arrowSize: 20});
                  //location.reload()
                  get_table_list();
                }  
              },
              error:function()
              {
                $('#preloader').css('display','none');
                $.notify("BOOM....!", "error");
              },
              complete:function()
              {
                $('#preloader').css('display','none');
              }
           });
    });
    </script>
@endsection