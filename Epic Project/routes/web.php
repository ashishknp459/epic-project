<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\userController;
use App\Http\Controllers\KanyeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('Auth.login');
})->name('auth.login');

Route::get('/register', function () {
    return view('Auth.register');
})->name('register');

Route::post('register',[userController::Class,'store'])->name('store');
Route::post('login',[userController::Class,'login'])->name('login');

Route::group(['middleware' => ['auth','admin_auth:Type|user']], function () {

Route::get('logout',[userController::Class,'logout'])->name('logout');
Route::group(array('prefix' => 'dashboard'), function(){
    Route::group(array('prefix' => 'kanye'), function(){
        Route::get('/',[KanyeController::Class,'index'])->name('kanye');
        Route::post('/get_kanye',[KanyeController::Class,'get_kanye']);
    });
    Route::group(array('prefix' => 'user'), function(){
        Route::get('/',[userController::Class,'index'])->name('index');
        Route::post('save',[userController::Class,'save']);
        Route::post('edit',[userController::Class,'edit']);
        Route::post('deleted',[userController::Class,'deletedata']);
        Route::post('editselectdata',[userController::Class,'edit_select']);
        Route::get('all_select',[userController::Class,'all_select']);
    });

});
});