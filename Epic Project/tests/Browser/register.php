<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Exception;

class register extends DuskTestCase
{
    /**
     * A Dusk test example.
     */
    public function testExample(): void
    {
        $this->browse(function (Browser $browser) {
        try{
            $browser->visit('http://127.0.0.1:8000/register');
            $browser ->pause(500);
            $browser ->typeSlowly('#name', 'ashish');
            $browser ->pause(500);
            $browser ->typeSlowly('#email', 'ashishpal@gmail.com');
            $browser ->pause(500);
            $browser ->typeSlowly('#password', 'ashish@123');
            $browser ->pause(500);
            $browser ->typeSlowly('#mobile_number', '1234567890');
            $browser ->pause(500);
            $browser ->press('#register');
            $browser ->pause(500);
            $browser->visit('http://127.0.0.1:8000/dashboard/');
            $browser ->pause(500);
            $browser ->assertSee('500');
            $browser ->pause(500);
        } catch(\Exception $e){
            echo 'Message: ' .$e->getMessage();
                echo "Failed :: Retry ";
                    throw new Exception('Registeration test case fail');                       
            }
            $browser->quit();
        });
    }
}
